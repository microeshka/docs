# Api description

### Status Codes

- `200` if request was successfully processed
- `201` if new resource was created as the result of this request
- `204` if some resource was deleted (no content)
- `400` if some field is not set or set to incorrect value
- `401` if session cookie does not present or not valid
- `403` if user is unauthenticated but do not have permission to the resource
- `404` if requested resource was not found (including being recently deleted)
- `500` if internal server error
- `502` if load balancer fails to communicate to backend service
- `504` if backend service does not reply to load balancer in time

### Auth & User

---

#### Login

Method: `POST`

Path: `/auth/login`

Body: `{ username, password }`

Response: `{ id, fullName, email, sessionId }`

---

#### Register

Method: `POST`

Path: `/auth/register`

Body: `{ username, email, fullName, password }`

Response: - (no content)

---

#### Get user base info

Method: `GET`

Path: `/user`

Response: `{ id, username, email, fullName }`

---

#### Get all supported permission

Method: `GET`

Path: `/auth/permissions`

Response: `[{ id, name }]`

---

### Group

---

#### Get basic group info

Method: `GET`

Path: `/group/:id`

Response: `{ id, name, description, createdAt, creatorId }`

---

#### Get group full info

Method: `GET`

Path: `/group/:id/full`

Response: `{ id, name, description, subjects: [{ id, title, description, groupId }], members: [{ id, fullName, email, username, roles: [{ id, name }] }]}, roles: [{ id, name, groupId, permissions: [{ id, name }] }] }`

---

#### Get group members

Method: `GET`

Path: `/group/:id/members`

Response: `[{ id, fullName, username, email, username, roles: [{ id, name }] }]`

---

#### Get group roles

Method: `GET`

Path: `/group/:id/roles`

Response: `[{ id, name, groupId, permissions: [{ id, name }] }]`

---

#### Get all user's group

Method: `GET`

Path: `/group/all`

Response: `[{ id, name, description }]`

---

#### Get some (see response signature) member statistic

Method: `GET`

Path: `/group/:id/profile/:userId/shared`

Response: `{ id, email, fullName, username, roles: [], groupsInCommon: [{ id, name ], authoredSubjects: [{ id, title }], authoredQueues: [{ id, title }] }`

---

#### Get user's roles in specific group

Method: `GET`

Path: `/group/:id/roles/:userId`

Response: `[string]`

---

#### Create new group

Method: `POST`

Path: `/group`

Body: `{ name, description }`

Response: `{ id, name, description, createdAt, creatorId }`

---

#### Update group info

Method: `POST`

Path: `/group`

Body: `{ id, name, description }`

Response: `{ id, name, description, createdAt, creatorId }`

---

#### Generate join-id

Method: `POST`

Path: `/group/:id/join-id`

Body: `{ force: bool }`

Response: `join-id` (just string that represents join-id)

If join-id does now exist then new one will be generated.
If join-id exist in the DB then it will be returned.
If `force` parameter if `true` then join-id will be regenerated and returned.

---

#### Join to the group

Method: `POST`

Path: `/group/:id/join`

Body: `{}`

Response: `Join success.`

---

#### Delete the group

Method: `DELETE`

Path: `/group/:id`

Response: - (no response)

---

### Subject

---

### Queue

---
