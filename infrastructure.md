# System Overview

For running all our services we use Kubernetes deployed to GCP GKE. Google Kubernetes Engine - secured and managed Kubernetes service with four-way auto-scaling and multi-cluster support.

### Infrastrusture

(*infrastructure diagram will be here*)

So far, we have two services:
* `auth` service provides login and register operations support.
* `frontend` service represents our site.

#### Kubernetes

All our app runs in one Kubernetes [cluster](https://kubernetes.io/docs/concepts/overview/components/). A Kubernetes cluster consists of a set of worker machines, called nodes, that run containerized applications. Every cluster has at least one worker node. All these nodes are controlled by the master.
We don't care about these nodes and what container on what node is deployed (master performs this work). Our task is to think about pods, services, and deployments.

A [Pod](https://kubernetes.io/docs/concepts/workloads/pods/) (as in a pod of whales or pea pod) is a group of one or more containers, with shared storage and network resources, and a specification for how to run the containers. A Pod's contents are always co-located and co-scheduled, and run in a shared context. **In out case one auth pod is equal to one auth container.**

An abstract way to expose an application running on a set of Pods as a network [service](https://kubernetes.io/docs/concepts/services-networking/service/). With Kubernetes you don't need to modify your application to use an unfamiliar service discovery mechanism. Kubernetes gives Pods their own IP addresses and a single DNS name for a set of Pods, and can load-balance across them. **In a few words, services use for networking. If we want to send a request to some service from outside, then we need a service with an external IP address.**

A [Deployment](https://kubernetes.io/docs/concepts/workloads/controllers/deployment/) provides declarative updates for Pods and ReplicaSets. In our case, we user deployments for creating and updating things like pods, services, etc.

#### Practise

This paragraph shows an example of deploying the auth service. Firstly, clone the auth repo:
```bash
git clone <auth-repo>
cd auth
```
Then login to Google SDK, set project, and install `kubectl`:
```bash
gcloud auth login
gcloud config set project <project-id>
gcloud components install kubectl
```
Then create GKE cluster:
```bash
gcloud container clusters create <cluster-name> --num-nodes=1 --enable-basic-auth --zone <your-gcp-zone>
gcloud container clusters get-credentials <cluster-name>
```
Then you need docker image of auth service. We'll get it in two steps:
1) create Dockerfile
2) publish docker image in GCP Build Registry

Dockerfile:
```Dockerfile
FROM node:lts-alpine3.13
COPY *.json ./
COPY yarn.lock ./
COPY src ./src
RUN yarn install && yarn build
EXPOSE 8080
CMD ["node", "build/index.js"]
```

Push image to registry:
```bash
gcloud builds submit --tag eu.gcr.io/<project-id>/<image-name> .
```

Now, time to deploy auth image to GKE. For reaching this we need to write a deployment file. `deploy/prod.yaml`:
```yaml
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: auth
  labels:
    app: auth
    department: auth-department
spec:
  replicas: 1
  selector:
    matchLabels:
      app: auth
      department: auth-department
  template:
    metadata:
      labels:
        app: auth
        department: auth-department
    spec:
      containers:
      - name: auth
        image: eu.gcr.io/micro-eshka/auth
        ports:
        - containerPort: 8080
          protocol: TCP
        env:
        - name: "PORT"
          value: "8080"
---
apiVersion: v1
kind: Service
metadata:
  name: auth
spec:
  type: NodePort
  selector:
    app: auth
    department: auth-department
  ports:
    - protocol: TCP
      port: 80
      targetPort: 8080
```

Then deploy and review created resources:
```bash
kubectl apply -f deploy/prod.yaml
kubectl expose deployment auth --type=LoadBalancer --name=auth-load-balancer
kubectl get nodes
kubectl get services
kubectl get deployments
```

For checking if all good run this command:
```bash
curl -X GET "httpd://<auth-load-balancer-ip>:<service-port>/ping"
```

Other userful links:
* [GKE quickstart](https://cloud.google.com/kubernetes-engine/docs/quickstarts/deploying-a-language-specific-app#node.js_1)

